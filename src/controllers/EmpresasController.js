const mongoose = require('mongoose')
const Empresa = mongoose.model('Empresa')

mongoose.set('useFindAndModify', false);

module.exports = {
    async getAll(req, res) {
        const registros = await Empresa.find({});
        return res.json(registros)
    },

    async getByID(req, res) {
        const registro = await Empresa.findById(req.params.id);
        return res.json(registro)
    },

    async incluir(req, res) {
        const jaExiste = await Empresa.find({ cpfCnpj: req.body.cpfCnpj });

        if (jaExiste.length == 0) {
            const registro = await Empresa.create(req.body)

            // Avisa o front-end
            req.io.emit('incluirEmpresa', registro);

            return res.json(registro)
        }

        return res.send()
    },

    async editar(req, res) {
        const registro = await Empresa.findByIdAndUpdate(req.params.id, req.body, { new: true });
        return res.json(registro)
    },

    async excluir(req, res) {
        const jaExiste = await Empresa.findById(req.params.id);

        if (jaExiste) {
            await Empresa.findByIdAndRemove(jaExiste._id);

            // Avisa o front-end
            req.io.emit('excluirEmpresa', jaExiste.cpfCnpj);
        }

        return res.send()
    },

    async excluirCpfCnpj(req, res) {
        const jaExiste = await Empresa.find({ cpfCnpj: req.params.cpfCnpj });

        if (jaExiste.length > 0) {
            const registro = await Empresa.findById(jaExiste[0]._id);

            await Empresa.findByIdAndRemove(registro._id);

            // Avisa o front-end
            req.io.emit('excluirEmpresa', req.params.cpfCnpj);
        }

        return res.send()
    },

    async refreshEmpresas(req, res) {
        // const colCpfCnpj = req.body.colCpfCnpj.map((obj) => { return obj.replace(/[^\d]+/g, '') })

        const colCpfCnpj = req.body.colCpfCnpj;
        const colRegistros = [];

        const colNovos = [];
        const colExcluir = [];

        const novosRegistros = await Empresa.find({ cpf: { "$nin": eval("[" + colCpfCnpj + "]") } });
        const jaExistemRegistros = await Empresa.find({ cpf: { "$in": eval("[" + colCpfCnpj + "]") } });

        if (novosRegistros.length > 0) {
            novosRegistros.map((empresaNovo) => {
                colNovos.push(empresaNovo)
            })
        }

        colCpfCnpj.map((regAtual) => {
            var itemEncontrado = jaExistemRegistros.filter((objJa) => { return objJa.cpf == regAtual })

            if (itemEncontrado.length == 0) {
                colExcluir.push(regAtual)
            }
        })

        if (colNovos.length > 0) {
            var ret = {
                "registro": "novo",
                "items": []
            }

            colNovos.map((obj) => {
                ret['items'].push(obj)
            })

            colRegistros.push(ret)
        }

        if (colExcluir.length > 0) {
            var ret = {
                "registro": "excluir",
                "items": []
            }

            colExcluir.map((obj) => {
                ret['items'].push(obj)
            })

            colRegistros.push(ret)
        }

        if (colRegistros.length > 0) {
            // Avisa o front-end
            req.io.emit('refreshEmpresas', colRegistros);

            return res.json(colRegistros)
        }

        return res.send()
    },
}