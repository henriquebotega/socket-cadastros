const mongoose = require('mongoose')
const Categoria = mongoose.model('Categoria')

mongoose.set('useFindAndModify', false);

module.exports = {
    async getAll(req, res) {
        const registros = await Categoria.find({});
        return res.json(registros)
    },

    async getByID(req, res) {
        const registro = await Categoria.findById(req.params.id);
        return res.json(registro)
    },

    async incluir(req, res) {
        const registro = await Categoria.create(req.body)
        return res.json(registro)
    },

    async editar(req, res) {
        const registro = await Categoria.findByIdAndUpdate(req.params.id, req.body, { new: true });
        return res.json(registro)
    },

    async excluir(req, res) {
        await Categoria.findByIdAndRemove(req.params.id);
        return res.send()
    }
}