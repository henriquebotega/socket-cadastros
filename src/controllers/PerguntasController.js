const mongoose = require('mongoose')
const Pergunta = mongoose.model('Pergunta')

mongoose.set('useFindAndModify', false);

module.exports = {
    async getAll(req, res) {
        const registros = await Pergunta.find({}).populate('_pesquisa');
        return res.json(registros)
    },

    async getAllByPesquisa(req, res) {
        await Pergunta.find({}).populate({ path: '_pesquisa', match: { _id: { $eq: req.params.idPesquisa } } })
            .exec(function (error, registros) {
                registros = registros.filter(function (regAtual) {
                    return regAtual._pesquisa != null;
                })

                return res.json(registros)
            })
    },

    async getByID(req, res) {
        const registro = await Pergunta.findById(req.params.id).populate('_pesquisa');
        return res.json(registro)
    },

    async incluir(req, res) {
        const registro = await Pergunta.create(req.body)
        return res.json(registro)
    },

    async editar(req, res) {
        const registro = await Pergunta.findByIdAndUpdate(req.params.id, req.body, { new: true }).populate('_pesquisa');
        return res.json(registro)
    },

    async excluir(req, res) {
        await Pergunta.findByIdAndRemove(req.params.id);
        return res.send()
    }
}