const mongoose = require('mongoose')
const Cliente = mongoose.model('Cliente')

mongoose.set('useFindAndModify', false);

module.exports = {
    async getAll(req, res) {
        const registros = await Cliente.find({});
        return res.json(registros)
    },

    async getByID(req, res) {
        const registro = await Cliente.findById(req.params.id);
        return res.json(registro)
    },

    async incluir(req, res) {
        const registro = await Cliente.create(req.body)
        return res.json(registro)
    },

    async editar(req, res) {
        const registro = await Cliente.findByIdAndUpdate(req.params.id, req.body, { new: true });
        return res.json(registro)
    },

    async excluir(req, res) {
        await Cliente.findByIdAndRemove(req.params.id);
        return res.send()
    }
}