const mongoose = require('mongoose')
const Resposta = mongoose.model('Resposta')

mongoose.set('useFindAndModify', false);

module.exports = {
    async getAll(req, res) {
        const registros = await Resposta.find({}).populate('_pesqempr').populate('_cliente');
        return res.json(registros)
    },

    async getByID(req, res) {
        const registro = await Resposta.findById(req.params.id).populate('_pesqempr').populate('_cliente');
        return res.json(registro)
    },

    async incluir(req, res) {
        const registro = await Resposta.create(req.body)
        return res.json(registro)
    },

    async editar(req, res) {
        const registro = await Resposta.findByIdAndUpdate(req.params.id, req.body, { new: true }).populate('_pesqempr').populate('_cliente');
        return res.json(registro)
    },

    async excluir(req, res) {
        await Resposta.findByIdAndRemove(req.params.id);
        return res.send()
    }
}