const mongoose = require('mongoose')
const Admin = mongoose.model('Admin')

mongoose.set('useFindAndModify', false);

module.exports = {
    async getAll(req, res) {
        const registros = await Admin.find({}).populate('_empresa');
        return res.json(registros)
    },

    async getByID(req, res) {
        const registro = await Admin.findById(req.params.id).populate('_empresa');
        return res.json(registro)
    },

    async incluir(req, res) {
        const registro = await Admin.create(req.body);
        return res.json(registro)
    },

    async editar(req, res) {
        const registro = await Admin.findByIdAndUpdate(req.params.id, req.body, { new: true }).populate('_empresa');
        return res.json(registro)
    },

    async excluir(req, res) {
        await Admin.findByIdAndRemove(req.params.id);
        return res.send()
    },

    async validar(req, res) {
        const registro = await Admin.find({ password: req.body.password, $or: [{ login: req.body.usuario }, { email: req.body.usuario }] }).populate('_empresa');
        return res.json(registro)
    }
}