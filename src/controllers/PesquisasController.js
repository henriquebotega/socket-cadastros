const mongoose = require('mongoose')
const Pesquisa = mongoose.model('Pesquisa')

mongoose.set('useFindAndModify', false);

module.exports = {
    async getAll(req, res) {
        const registros = await Pesquisa.find({}).populate('_categoria');
        return res.json(registros)
    },

    async getByID(req, res) {
        const registro = await Pesquisa.findById(req.params.id).populate('_categoria');
        return res.json(registro)
    },

    async incluir(req, res) {
        const registro = await Pesquisa.create(req.body);
        return res.json(registro)
    },

    async editar(req, res) {
        const registro = await Pesquisa.findByIdAndUpdate(req.params.id, req.body, { new: true }).populate('_categoria');
        return res.json(registro)
    },

    async excluir(req, res) {
        await Pesquisa.findByIdAndRemove(req.params.id);
        return res.send()
    }
}