const mongoose = require('mongoose')
const PesqEmpr = mongoose.model('PesqEmpr')

mongoose.set('useFindAndModify', false);

module.exports = {
    async getAll(req, res) {
        const registros = await PesqEmpr.find({}).populate('_pesquisa').populate('_empresa');
        return res.json(registros)
    },

    async getAllByPesquisa(req, res) {
        await PesqEmpr.find({}).populate({ path: '_pesquisa', match: { _id: { $eq: req.params.idPesquisa } } }).populate('_empresa')
            .exec(function (error, registros) {
                registros = registros.filter(function (regAtual) {
                    return regAtual._pesquisa != null;
                })

                return res.json(registros)
            })
    },

    async getByID(req, res) {
        const registro = await PesqEmpr.findById(req.params.id).populate('_pesquisa').populate('_empresa');
        return res.json(registro)
    },

    async incluir(req, res) {
        const registro = await PesqEmpr.create(req.body);
        return res.json(registro)
    },

    async editar(req, res) {
        const registro = await PesqEmpr.findByIdAndUpdate(req.params.id, req.body, { new: true }).populate('_pesquisa').populate('_empresa');
        return res.json(registro)
    },

    async excluir(req, res) {
        await PesqEmpr.findByIdAndRemove(req.params.id);
        return res.send()
    }
}