const mongoose = require('mongoose')

const PerguntaSchema = new mongoose.Schema({
    titulo: { type: String, required: true },
    _pesquisa: { type: mongoose.Schema.Types.ObjectId, ref: 'Pesquisa' }
}, { timestamps: true })

mongoose.model('Pergunta', PerguntaSchema)