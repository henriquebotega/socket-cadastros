const mongoose = require('mongoose')

const EmpresaSchema = new mongoose.Schema({
    nome: { type: String, required: true },
    razaoSocial: { type: String, required: true },
    cpfCnpj: { type: String, required: true, unique: true },
    email: String,
}, { timestamps: true })

mongoose.model('Empresa', EmpresaSchema)