const mongoose = require('mongoose')

const RespostaSchema = new mongoose.Schema({
    _pesqempr: { type: mongoose.Schema.Types.ObjectId, ref: 'PesqEmpr' },
    _cliente: { type: mongoose.Schema.Types.ObjectId, ref: 'Cliente' },
    conteudo: { type: String, required: true },
}, { timestamps: true })

mongoose.model('Resposta', RespostaSchema)