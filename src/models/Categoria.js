const mongoose = require('mongoose')

const CategoriaSchema = new mongoose.Schema({
    titulo: { type: String, required: true }
}, { timestamps: true })

mongoose.model('Categoria', CategoriaSchema)