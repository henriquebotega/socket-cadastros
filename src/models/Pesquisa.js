const mongoose = require('mongoose')

const PesquisaSchema = new mongoose.Schema({
    titulo: { type: String, required: true },
    _categoria: { type: mongoose.Schema.Types.ObjectId, ref: 'Categoria' }
}, { timestamps: true })

mongoose.model('Pesquisa', PesquisaSchema)