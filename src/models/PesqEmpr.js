const mongoose = require('mongoose')

const PesqEmprSchema = new mongoose.Schema({
    _pesquisa: { type: mongoose.Schema.Types.ObjectId, ref: 'Pesquisa' },
    _empresa: { type: mongoose.Schema.Types.ObjectId, ref: 'Empresa' }
}, { timestamps: true })

mongoose.model('PesqEmpr', PesqEmprSchema)