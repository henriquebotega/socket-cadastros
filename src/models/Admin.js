const mongoose = require('mongoose')

const AdminSchema = new mongoose.Schema({
    login: { type: String, required: true },
    password: { type: String, required: true },
    email: { type: String, required: true },
    _empresa: { type: mongoose.Schema.Types.ObjectId, ref: 'Empresa' }
}, { timestamps: true })

mongoose.model('Admin', AdminSchema)