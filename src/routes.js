const express = require('express')
const routes = express.Router()

const AdminsController = require('./controllers/AdminsController')
routes.get('/admins', AdminsController.getAll);
routes.get('/admins/:id', AdminsController.getByID);
routes.post('/admins', AdminsController.incluir);
routes.put('/admins/:id', AdminsController.editar);
routes.delete('/admins/:id', AdminsController.excluir);
routes.post('/admins/validar', AdminsController.validar);

const EmpresasController = require('./controllers/EmpresasController')
routes.get('/empresas', EmpresasController.getAll);
routes.get('/empresas/:id', EmpresasController.getByID);
routes.post('/empresas', EmpresasController.incluir);
routes.put('/empresas/:id', EmpresasController.editar);
routes.delete('/empresas/:id', EmpresasController.excluir);
routes.delete('/empresas/cpfCnpj/:cpfCnpj', EmpresasController.excluirCpfCnpj);
routes.post('/empresas/refresh', EmpresasController.refreshEmpresas);

const CategoriasController = require('./controllers/CategoriasController')
routes.get('/categorias', CategoriasController.getAll);
routes.get('/categorias/:id', CategoriasController.getByID);
routes.post('/categorias', CategoriasController.incluir);
routes.put('/categorias/:id', CategoriasController.editar);
routes.delete('/categorias/:id', CategoriasController.excluir);

const PesquisasController = require('./controllers/PesquisasController')
routes.get('/pesquisas', PesquisasController.getAll);
routes.get('/pesquisas/:id', PesquisasController.getByID);
routes.post('/pesquisas', PesquisasController.incluir);
routes.put('/pesquisas/:id', PesquisasController.editar);
routes.delete('/pesquisas/:id', PesquisasController.excluir);

const PesqEmprController = require('./controllers/PesqEmprController')
routes.get('/pesqempr', PesqEmprController.getAll);
routes.get('/pesqempr/pesquisa/:idPesquisa', PesqEmprController.getAllByPesquisa);
routes.get('/pesqempr/:id', PesqEmprController.getByID);
routes.post('/pesqempr', PesqEmprController.incluir);
routes.put('/pesqempr/:id', PesqEmprController.editar);
routes.delete('/pesqempr/:id', PesqEmprController.excluir);

const ClientesController = require('./controllers/ClientesController')
routes.get('/clientes', ClientesController.getAll);
routes.get('/clientes/:id', ClientesController.getByID);
routes.post('/clientes', ClientesController.incluir);
routes.put('/clientes/:id', ClientesController.editar);
routes.delete('/clientes/:id', ClientesController.excluir);

const PerguntasController = require('./controllers/PerguntasController')
routes.get('/perguntas', PerguntasController.getAll);
routes.get('/perguntas/pesquisa/:idPesquisa', PerguntasController.getAllByPesquisa);
routes.get('/perguntas/:id', PerguntasController.getByID);
routes.post('/perguntas', PerguntasController.incluir);
routes.put('/perguntas/:id', PerguntasController.editar);
routes.delete('/perguntas/:id', PerguntasController.excluir);

const RespostasController = require('./controllers/RespostasController')
routes.get('/respostas', RespostasController.getAll);
routes.get('/respostas/:id', RespostasController.getByID);
routes.post('/respostas', RespostasController.incluir);
routes.put('/respostas/:id', RespostasController.editar);
routes.delete('/respostas/:id', RespostasController.excluir);

module.exports = routes