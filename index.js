const express = require('express')
const requireDir = require('require-dir')
const cors = require('cors')
const mongoose = require('mongoose')
const bodyParser = require('body-parser');

mongoose.connect('mongodb+srv://usuario:senha@banco-m79fh.mongodb.net/test?retryWrites=true', { useCreateIndex: true, useNewUrlParser: true })

const app = express()
app.use(cors())

const server = require('http').Server(app);
const io = require('socket.io')(server);

app.use((req, res, next) => {
    req.io = io;
    return next();
})

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }));

requireDir('./src/models')
app.use('/api', require('./src/routes'))

server.listen(process.env.PORT || 4000, () => {
    console.log('Server is running on port 4000... :)')
})